# This is a multistage build.

# Helper image for installing tools we need
# hadolint ignore=DL3007
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master AS installer

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

# DL3018: explicitly not pin apk versions: we need upgrades here
# hadolint ignore=DL3018
RUN echo "building intermediate image: installer" \
	&& apk -qq --no-cache add \
		binutils \
		binutils-aarch64-none-elf \
		file \
		gcc \
		musl-dev \
		openssl-dev \
		pkgconf \
	# install latest rust
	&& curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs \
		| bash -s -- -y --profile minimal \
	# install rust additional tools
	&& "${HOME}/.cargo/bin/rustup" component add clippy \
	# install cargo additional tools
	&& "${HOME}/.cargo/bin/cargo" install \
		cargo-auditable \
	# strip the elfs
	&& find \
		/root/.cargo/bin/ \
		/root/.rustup/toolchains/ \
			-type f \
			-executable \
			-print0 \
		| xargs -0 file \
		| awk -F ':' '/not stripped/ {print $1}' \
		| xargs strip -v \
		# TODO: strip the libs?
	# install other tools here
	# ...
	# cleanup
	&& find /tmp/ \
		-mindepth 1 \
		-delete \
	&& echo "done"

# This is our final image, based on base image
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master

LABEL maintaner="Ilya A. Frolov <if+gitlab@solas.is>" \
      description="Image we use as a builder image for go tools"

ENV CARGO_HOME="/usr/local" \
    RUSTUP_HOME="/usr/local/.rustup" \
    OPENSSL_DIR="/usr"

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

COPY --from=installer --chown=root:root --chmod=755 \
	/root/.cargo/bin/* \
	/usr/local/bin/

COPY --from=installer --chown=root:root \
	/root/.rustup/ \
	/usr/local/.rustup/

# hadolint ignore=DL3018,SC2016
RUN \
	# We bind-mount scripts and tests so that we don't pollute the end image
	# NOTE: instead of mounting separate dirs, we mount both so that we don't have
	# empty /mnt/scripts and /mnt/tests directory leftovers in the image :ocd:
	--mount=type=bind,source=.gitlab.d/ci,target=/mnt,readonly \
	# We also bind-mount goss and trivy binaries from the docker-builder image
	--mount=type=bind,source=goss,target=/bin/goss,readonly \
	--mount=type=bind,source=trivy,target=/bin/trivy,readonly \
	# And we also bind-mount .trivyignore file
	--mount=type=bind,source=.trivyignore,target=/.trivyignore,readonly \
	# Main flow below
	apk add --no-cache \
		binutils \
		binutils-aarch64-none-elf \
		file \
		gawk \
		gcc \
		git \
		gnupg \
		openssl-dev \
		pkgconf \
		tar \
		unzip \
		xz \
	# cleanup
	&& find /tmp/ \
		-mindepth 1 \
		-delete \
	# at this point, the _build_ is done, and we proceed to run _tests_ from within the image,
	# in order to abort the whole thing if they fail, and never push anything unsafe to the registry
	# these stages should be the last, and they should also be self-contained, i.e. do not install
	# any dependencies to not pollute the end image
	&& bash /mnt/scripts/skkrty_inside_build.sh \
	&& bash /mnt/scripts/tests_inside_build.sh \
	&& echo "done"

CMD ["/bin/bash"]
